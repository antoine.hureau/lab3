package no.uib.inf101.terminal;

public interface Command {

    default void setContext(Context context) { /* do nothing */ };

    String run(String[] args);

    String getName();

}
